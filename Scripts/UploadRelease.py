import os
import json
import hashlib

import pysftp

from pathlib import Path, PurePosixPath

ROOT = "../Build/Release/"
OUTPUT = "./clienthash.json"

REMOTE_HASH = '/var/www/api.b2soft.org/mary-beta/storage/client/clienthash.json'
REMOTE_DIR = '/var/www/api.b2soft.org/mary-beta/storage/client/active'



print("Start uploading...")

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None
sftp = pysftp.Connection(host="b2soft.org", username="b2soft", password="aZQ105dx3826459", log="./pysftp.log", cnopts=cnopts)

print('Remove old files')

def rm(path):
	files = sftp.listdir(path)

	for f in files:
		filepath = path + '/' + f
		try:
			sftp.remove(filepath)
		except IOError:
			rm(filepath)

	sftp.rmdir(path)
try:
	rm(REMOTE_DIR)
except:
	pass



print('Making new dir')
sftp.mkdir(REMOTE_DIR, 755)
outList = []

for root, dirs, files in os.walk(ROOT):
	for fpath in [os.path.join(root, f) for f in files]:
		size = os.path.getsize(fpath)
		with open(fpath, 'rb') as f:
			sha = hashlib.sha1(f.read())
			name = os.path.relpath(fpath, ROOT)
			print('Uploading ' + name)
			remote_dir = REMOTE_DIR + '/' + PurePosixPath(Path(os.path.relpath(root, ROOT))).as_posix()
			posixPath = PurePosixPath(Path(name))

			if ( Path(name).suffix == "dat") or (Path(name).name == "vc_redist.x64.exe"):
				continue

			try:
				sftp.put(f.name, REMOTE_DIR + '/' + posixPath.as_posix())
			except FileNotFoundError:
				sftp.makedirs(remote_dir, 755)
				sftp.put(ROOT + posixPath.as_posix(), REMOTE_DIR + '/' + posixPath.as_posix())
			

			fileData = {"path" : posixPath.as_posix(), "hash": sha.hexdigest(), "mode":"EXT" }
			outList.append(fileData)

print("Writing Hashes")

with open(OUTPUT, 'w') as outFile:
	json.dump(outList, outFile)

outFile.close()

print("Uploading Hashes")
sftp.put(OUTPUT, REMOTE_HASH)

sftp.close()
# Closes the connection
