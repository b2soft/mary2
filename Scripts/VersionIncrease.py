import re

num = 0
header = open('../Sources/version.h', 'r')
initial = header.readlines()
outer = []
header.close()
header2 = open('../Sources/version.h', 'w')

for line in initial:
    result = re.search('BUILD_NUMBER', line)
    if result:
        num = int(re.findall('\d+', line)[0])
        num = num + 1
        header2.write('#define BUILD_NUMBER ' + str(num) + '\n')
    else:
        header2.write(line)

header2.close()

print('BUILD_NUMBER set ' + str(num))
