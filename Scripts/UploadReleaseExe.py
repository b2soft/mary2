import os
import json
import hashlib

import pysftp

from pathlib import Path, PurePosixPath

ROOT = "../Build/Release/"
OUTPUT = "./clienthash.json"

REMOTE_HASH = '/var/www/api.b2soft.org/mary-beta/storage/client/clienthash.json'
REMOTE_DIR = '/var/www/api.b2soft.org/mary-beta/storage/client/active'



print("Start uploading...")

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None
sftp = pysftp.Connection(host="b2soft.org", username="b2soft", password="aZQ105dx3826459", log="./pysftp.log", cnopts=cnopts)

print('Remove old exe')

	
try:
	sftp.remove( REMOTE_DIR + '/Mary.exe' )
except:
	pass


outList = []

for root, dirs, files in os.walk(ROOT):
	for fpath in [os.path.join(root, f) for f in files]:
		size = os.path.getsize(fpath)
		with open(fpath, 'rb') as f:
			sha = hashlib.sha1(f.read())
			name = os.path.relpath(fpath, ROOT)
			remote_dir = REMOTE_DIR + '/' + PurePosixPath(Path(os.path.relpath(root, ROOT))).as_posix()
			posixPath = PurePosixPath(Path(name))
			if ( Path(name).name == "Mary.exe"):
				try:
					print('Uploading ' + name)
					sftp.put(f.name, REMOTE_DIR + '/' + posixPath.as_posix())
				except FileNotFoundError:
					sftp.makedirs(remote_dir, 755)
					sftp.put(ROOT + posixPath.as_posix(), REMOTE_DIR + '/' + posixPath.as_posix())
			

			fileData = {"path" : posixPath.as_posix(), "hash": sha.hexdigest(), "mode":"EXT" }
			outList.append(fileData)

print("Writing Hashes")

with open(OUTPUT, 'w') as outFile:
	json.dump(outList, outFile)

outFile.close()

print("Uploading Hashes")
sftp.put(OUTPUT, REMOTE_HASH)

sftp.close()
# Closes the connection
