import os
import json
import hashlib

from pathlib import Path, PurePosixPath

ROOT = "../Build/Release/"
OUTPUT = "./clienthash.json"

outList = []

for root, dirs, files in os.walk(ROOT):
	for fpath in [os.path.join(root, f) for f in files]:
		size = os.path.getsize(fpath)
		with open(fpath, 'rb') as f:
			sha = hashlib.sha1(f.read())
			name = os.path.relpath(fpath, ROOT)
			posixPath = PurePosixPath(Path(name))
			fileData = {"path" : posixPath.as_posix(), "hash": sha.hexdigest(), "mode":"EXT" }
			outList.append(fileData)

with open(OUTPUT, 'w') as outFile:	json.dump(outList, outFile)

outFile.close()