import QtQuick 2.11

import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0

import org.b2soft.qml 1.0

ApplicationWindow {
    id: window
    visible: true
    title: qsTr("Mary")
    width: 800
    height: 600

    color: "#00000000"
    flags: Qt.FramelessWindowHint | Qt.Window

    DropShadow {
        anchors.fill: stackView
        radius: 40
        samples: 32
        verticalOffset: 14
        source: mainScreen
        color: "#40000000"
    }

    StackView {
        id: stackView
        width: 700
        height: 500
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter


        Rectangle {
            id: mainScreen
            color: "#ffffff"
            visible: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.fill: parent

            Connections {
                target: UserHandler
                onLoginCompleted: rootStackView.push(Qt.resolvedUrl(
                                                         "main_layout.qml"))
            }

            Rectangle {
                id: topBar
                width: 700
                height: 24
                color: "#37373a"

                MouseArea {
                    anchors.fill: parent;
                    property variant clickPos: "1,1"

                    onPressed: {
                        clickPos = Qt.point(mouse.x,mouse.y)
                    }

                    onPositionChanged: {
                        var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
                        window.x += delta.x
                        window.y += delta.y
                    }
                }

                Image {
                    id: logo
                    width: 70
                    height: 24
                    source: "qrc:/img/logo.png"
                }

                Button {
                    id: minimizeButton
                    width: 24
                    height: 24
                    anchors.right: closeButton.left
                    anchors.rightMargin: 0
                    onClicked: window.showMinimized()
                    background: Image {
                        source: "qrc:/img/minimize.png"
                    }
                }

                Button {
                    id: closeButton
                    width: 24
                    height: 24
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    onClicked: Qt.quit()
                    background: Image {
                        source: "qrc:/img/close.png"
                    }
                }
            }

            StackView {
                id: rootStackView
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: topBar.bottom
                anchors.topMargin: 0
                clip: true

                initialItem: Qt.resolvedUrl("login.qml")

                Connections {
                    target: QMLWrapper
                    onSelfUpdated: {
                        mainScreen.visible = true
                        opacityAnimator.running = true
                    }
                }
            }


        }

        Rectangle {
            id: splashScreen
            color: "#37373a"
            anchors.fill: parent

            Image {
                id: image
                width: 350
                height: 150
                anchors.top: parent.top
                anchors.topMargin: 80
                anchors.horizontalCenter: parent.horizontalCenter
                source: "qrc:/img/logo_big.png"
            }

            Text {
                id: text1
                color: "#ffffff"
                text: qsTr("Self-Update in progress")
                font.weight: Font.ExtraLight
                font.family: "ArialRoboto"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 80
                font.pixelSize: 24
            }

            OpacityAnimator on opacity{
                id: opacityAnimator
                from: 1.0
                to: 0.0
                duration: 500
                running: false

                onStopped: {
                    splashScreen.visible = false
                }
            }
        }
    }
}
