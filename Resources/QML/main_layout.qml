import QtQuick 2.0
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0

import org.b2soft.qml 1.0

Page {
    id: page
    width: 700
    height: 476

    Rectangle {
        id: topBarMain
        height: 71
        color: "white"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        function updateTime() {
            var localTime = new Date()
            var utcTime = new Date( localTime.getTime() + localTime.getTimezoneOffset() * 60000 )

            localTimeText.text = Qt.formatDateTime( localTime, "hh:mm:ss" )
            utcTimeText.text = Qt.formatDateTime( utcTime, "hh:mm:ss" )

            //if ( dateTime.getSeconds() % 2 ) {
            //localTimeText.text = Qt.formatDateTime( dateTime, "hh:mm:ss" )
            //utcTimeText.text = Qt.formatDateTime( utcTime, "hh:mm:ss" )
            //} else {
            //	localTimeText.text = Qt.formatDateTime( dateTime, "hh mm ss" )
            //	utcTimeText.text = Qt.formatDateTime( dateTime, "hh mm ss" )
            //}
        }

        Component.onCompleted: updateTime()

        Timer {
            interval: 1000; running: true; repeat: true
            onTriggered: {
                topBarMain.updateTime()
            }
        }

        Rectangle {
            id: rectangle
            height: 1
            color: "#d9dee3"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
        }

        Item {
            id: vatsimIdPlaceholder
            width: 175
            height: 40
            anchors.verticalCenter: parent.verticalCenter

            Text {
                id: vatsimLabel
                color: "#aeb2b5"
                text: qsTr("VATSIM ID")
                font.pixelSize: 13
                fontSizeMode: Text.Fit
                textFormat: Text.PlainText
                verticalAlignment: Text.AlignTop
                horizontalAlignment: Text.AlignLeft
                font.letterSpacing: 1.5
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Medium
                style: Text.Normal
                font.family: "Roboto"
            }

            Text {
                id: vatsimIdText
                color: "#000000"
                text: UserHandler.vatsimId
                elide: Text.ElideNone
                wrapMode: Text.NoWrap
                antialiasing: true
                smooth: true
                font.pixelSize: 15
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Light
                style: Text.Normal
                font.family: "Roboto"
            }
        }

        Item {
            id: ratingPlaceholder
            width: 175
            height: 40
            anchors.left: vatsimIdPlaceholder.right
            anchors.leftMargin: 0
            Text {
                id: ratingLabel
                color: "#aeb2b5"
                text: qsTr("RATING")
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Medium
                style: Text.Normal
                font.pixelSize: 13
                font.letterSpacing: 1.5
                font.family: "Roboto"
            }

            Text {
                id: ratingText
                color: "#000000"
                text: UserHandler.rating
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Light
                style: Text.Normal
                font.pixelSize: 16
                anchors.bottomMargin: 0
                anchors.bottom: parent.bottom
                font.family: "Roboto"
            }

            Rectangle {
                id: rectangle1
                width: 1
                color: "#e3e3e3"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
            }
            anchors.verticalCenter: parent.verticalCenter
        }

        Item {
            id: utcTimePlaceholder
            width: 175
            height: 40
            anchors.left: ratingPlaceholder.right
            anchors.leftMargin: 0
            Text {
                id: utcTimeLabel
                color: "#aeb2b5"
                text: qsTr("UTC TIME")
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Medium
                style: Text.Normal
                font.pixelSize: 13
                font.letterSpacing: 1.5
                font.family: "Roboto"
            }

            Text {
                id: utcTimeText
                color: "#000000"
                text: qsTr("14:06:01")
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Light
                style: Text.Normal
                font.pixelSize: 15
                anchors.bottomMargin: 0
                anchors.bottom: parent.bottom
                font.family: "Roboto"
            }

            Rectangle {
                id: rectangle2
                width: 1
                color: "#e3e3e3"
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottomMargin: 0
                anchors.bottom: parent.bottom
                anchors.top: parent.top
            }
            anchors.verticalCenter: parent.verticalCenter
        }

        Item {
            id: localTimePlaceholder
            width: 175
            height: 40
            anchors.left: utcTimePlaceholder.right
            anchors.leftMargin: 0
            Text {
                id: localTimeLabel
                color: "#aeb2b5"
                text: qsTr("LOCAL TIME")
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Medium
                style: Text.Normal
                font.pixelSize: 13
                font.letterSpacing: 1.5
                font.family: "Roboto"
            }

            Text {
                id: localTimeText
                color: "#000000"
                text: qsTr("11:06:01")
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Light
                style: Text.Normal
                font.pixelSize: 15
                anchors.bottomMargin: 0
                anchors.bottom: parent.bottom
                font.family: "Roboto"
            }

            Rectangle {
                id: rectangle3
                width: 1
                color: "#e3e3e3"
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottomMargin: 0
                anchors.bottom: parent.bottom
                anchors.top: parent.top
            }
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    SwipeView {
        id: mainSwipeView
        anchors.bottomMargin: 0
        anchors.top: topBarMain.bottom
        anchors.right: parent.right
        anchors.bottom: bottomBarMain.top
        anchors.left: parent.left
        anchors.topMargin: 0

        interactive: false

        Loader {
            source: "dashboard.qml"
        }

        Loader {
            source: "update.qml"
        }

        Loader {
            source: "settings.qml"
        }

    }

    Rectangle {
        id: bottomBarMain
        height: 46
        color: "white"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0


        Item {
            id: dashboardPlaceholder
            width: 130
            height: 46
            Image {
                id: icon1
                width: 16
                height: 16
                anchors.left: parent.left
                anchors.leftMargin: 16
                anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/img/home.png"
            }

            ColorOverlay {
                anchors.fill: icon1
                source: icon1
                color: mainSwipeView.currentIndex == 0 ? "#4f4f50" : "#929294"
            }

            Text {
                id: dashboardLabel
                height: 14
                color: mainSwipeView.currentIndex == 0 ? "#4f4f50" : "#656464"
                text: qsTr("Dashboard")
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 14
                font.family: "Roboto"
                anchors.right: parent.right
                font.weight: Font.Normal
                anchors.rightMargin: 16
            }

            Rectangle {
                id: rectangle5
                width: 1
                height: 0
                color: "#ebebeb"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.topMargin: 0
                anchors.bottomMargin: 0
                anchors.rightMargin: 0
            }

            MouseArea {
                id: dashboardMouseArea
                anchors.fill: parent

                onClicked: mainSwipeView.currentIndex = 0
            }

            Rectangle {
                id: activeDashboardMarker
                height: 4
                color: "#2c599e"
                visible: mainSwipeView.currentIndex == 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
            }
        }


        Item {
            id: updatePlaceholder
            width: 130
            height: 46
            Image {
                id: icon4
                width: 16
                height: 16
                anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/img/update.png"
                anchors.leftMargin: 16
                anchors.left: parent.left
            }

            ColorOverlay {
                color: mainSwipeView.currentIndex == 1 ? "#4f4f50" : "#929294"
                anchors.fill: icon4
                source: icon4
            }

            Text {
                id: updateLabel
                height: 14
                color: mainSwipeView.currentIndex == 1 ? "#4f4f50" : "#656464"
                text: qsTr("Update")
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 31
                font.weight: Font.Normal
                font.family: "Roboto"
                font.pixelSize: 14
                anchors.right: parent.right
            }

            Rectangle {
                id: rectangle8
                width: 1
                height: 0
                color: "#ebebeb"
                anchors.bottom: parent.bottom
                anchors.topMargin: 0
                anchors.rightMargin: 0
                anchors.top: parent.top
                anchors.bottomMargin: 0
                anchors.right: parent.right
            }

            MouseArea {
                id: updateMouseArea
                anchors.fill: parent

                onClicked: mainSwipeView.currentIndex = 1
            }

            Rectangle {
                id: activeUpdateMarker
                height: 4
                color: "#2c599e"
                anchors.topMargin: 0
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                visible: mainSwipeView.currentIndex == 1
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
            }
            anchors.leftMargin: 0
            anchors.left: dashboardPlaceholder.right
        }

        Item {
            id: settingsPlaceholder
            width: 130
            height: 46
            anchors.left: updatePlaceholder.right
            anchors.leftMargin: 0

            Image {
                id: icon
                width: 16
                height: 16
                anchors.left: parent.left
                anchors.leftMargin: 16
                anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/img/settings.png"
            }

            ColorOverlay {
                anchors.fill: icon
                source: icon
                color: mainSwipeView.currentIndex == 2 ? "#4f4f50" : "#929294"
            }

            Text {
                id: settingsLabel
                height: 14
                text: qsTr("Settings")
                color: mainSwipeView.currentIndex == 2 ? "#4f4f50" : "#656464"
                anchors.right: parent.right
                anchors.rightMargin: 16
                font.weight: Font.Normal
                font.family: "Roboto"
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 14
            }

            Rectangle {
                id: rectangle4
                width: 1
                height: 0
                color: "#ebebeb"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
            }

            MouseArea {
                id: settingsMouseArea
                anchors.fill: parent

                onClicked: mainSwipeView.currentIndex = 2
            }

            Rectangle {
                id: activeSettingsMarker
                height: 4
                color: "#2c599e"
                visible: mainSwipeView.currentIndex == 2
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.rightMargin: 0
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.right: parent.right
            }

        }



        Rectangle {
            height: 1
            color: "#d9dee3"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
        }



        Item {
            id: contactPlaceholder
            width: 130
            height: 46
            anchors.right: aboutPlaceholder.left
            anchors.rightMargin: 0
            Image {
                id: icon2
                width: 18
                height: 15
                anchors.left: parent.left
                anchors.leftMargin: 16
                anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/img/contact.png"
            }

            Text {
                id: contactLabel
                height: 14
                color: "#656464"
                text: qsTr("Contact Us")
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 14
                font.family: "Roboto"
                anchors.right: parent.right
                font.weight: Font.Light
                anchors.rightMargin: 16
            }

            Rectangle {
                id: rectangle6
                width: 1
                color: "#ebebeb"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
            }
        }



        Item {
            id: aboutPlaceholder
            width: 130
            height: 46
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 0
            Image {
                id: icon3
                width: 16
                height: 16
                anchors.left: parent.left
                anchors.leftMargin: 32
                anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/img/about.png"
            }

            Text {
                id: aboutLabel
                height: 14
                color: "#656464"
                text: qsTr("About")
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 14
                font.family: "Roboto"
                anchors.right: parent.right
                font.weight: Font.Light
                anchors.rightMargin: 32
            }

            Rectangle {
                id: rectangle7
                width: 1
                height: 0
                color: "#ebebeb"
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: 0
                anchors.bottomMargin: 0
            }
        }



    }

}

