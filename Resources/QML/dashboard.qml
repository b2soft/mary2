import QtQuick 2.0
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0

import org.b2soft.qml 1.0

Page {
    id: page
    width: 700
    height: 360

    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: 700
        height: 360
        color: "#ffffff"
        border.width: 0

        Image {
            id: image
            x: 0
            y: 0
            width: 700
            height: 360
            source: "qrc:/img/dashboard_bg.png"
        }
    }



    Item {
        id: buttonsItem
        width: 200
        height: 120
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        Button {
            id: launchEuroscopeButton
            width: 255
            height: 50
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            font.family: "Roboto"
            font.weight: "Light"

            onClicked: {
                ApplicationHandler.startEuroscope();
            }

            background: Rectangle {
                width: parent.width
                height: parent.height
                radius: 25
                border.color: "#3846b0"
                gradient: Gradient {
                    GradientStop {
                        position: 0.0
                        color: "#4253d0"
                    }
                    GradientStop {
                        position: 1.0
                        color: "#3744ac"
                    }
                }

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 0
                    verticalOffset: 8
                    radius: 25
                    color: "#66111e71"
                    samples: 32
                }
            }

            contentItem: Item {
                id: esItem
                Image {
                    id: euroscopeIcon
                    width: 23
                    height: 23
                    anchors.left: parent.left
                    anchors.leftMargin: 24
                    anchors.verticalCenter: parent.verticalCenter
                    source: "qrc:/img/es_logo.png"
                }

                Text {
                    color: launchEuroscopeButton.down ? "white" : "white"
                    text: "Launch Euroscope"
                    font.pixelSize: 19
                    anchors.right: parent.right
                    anchors.rightMargin: 24
                    anchors.left: euroscopeIcon.right
                    anchors.leftMargin: 11
                    anchors.verticalCenter: parent.verticalCenter
                    font.weight: Font.Light
                    font.family: "Roboto"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }

        Button {
            id: launchDiscordButton
            width: 190
            height: 42
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            contentItem: Item {
                id: esItem1
                Image {
                    id: discordIcon
                    width: 21
                    height: 15
                    source: "qrc:/img/discord_logo.png"
                    anchors.left: parent.left
                    anchors.leftMargin: 18
                    anchors.verticalCenter: parent.verticalCenter
                }

                Text {
                    color: launchDiscordButton.down ? "white" : "white"
                    text: "Launch Discord"
                    textFormat: Text.PlainText
                    anchors.rightMargin: 18
                    font.weight: Font.Light
                    anchors.left: discordIcon.right
                    font.pixelSize: 16
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.leftMargin: 8
                    font.family: "Roboto"
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
            font.weight: "Light"
            background: Rectangle {
                width: parent.width
                height: parent.height
                radius: 25
                layer.enabled: true
                border.color: "#4867d4"
                layer.effect: DropShadow {
                    color: "#66111e71"
                    radius: 19
                    horizontalOffset: 0
                    verticalOffset: 3
                    samples: 32
                }
                gradient: Gradient {
                    GradientStop {
                        position: 0
                        color: "#4766d3"
                    }

                    GradientStop {
                        position: 1
                        color: "#405bbd"
                    }
                }
            }
            font.family: "Roboto"
        }

    }
}
