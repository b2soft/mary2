import QtQuick 2.11
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3

import org.b2soft.qml 1.0

Page {
    id: page
    width: 700
    height: 360

    Rectangle {
        id: rectangle
        width: 700
        height: 360
        color: "#ffffff"
        border.width: 0

        ProgressBar {
            id: progressBar
            x: 79
            y: 147
            width: 562
            height: 77
            spacing: 0
            wheelEnabled: false
            font.pointSize: 8
            value: 0.0
            visible: false

            Connections {
                target: UpdateHandler
                onUpdateProgress: {
                    progressBar.value = current / count
                }
            }
        }
    }

    Connections {
        target: UpdateHandler
        onCheckoutComplete: {
            checkButton.enabled = true
            if ( needsUpdate ) {
                textDebug.text = "Ready to Update"
                updateButton.enabled = true
            } else {
                textDebug.text = "Up-to-date"
            }
        }

        onUpdateCompleted: {
            textDebug.text = "Up-to-date"
            checkButton.enabled = true
            progressBar.visible = false
        }

        onSectorFolderError: {
            textDebug.text = "Sector folder error! Check sector path in the Settings!"
            checkButton.enabled = true
        }
    }


    Item {
        id: textContainer
        width: 540
        height: 55
        anchors.top: parent.top
        anchors.topMargin: 64
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: textDebug
            height: 0
            color: "#6d6d6d"
            text: "Press 'Check' to check update"
            font.weight: Font.Light
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            font.family: "Roboto"
            font.pixelSize: 20
        }
    }

    Button {
        id: updateButton
        x: 465
        y: 244
        width: 174
        height: 50
        text: "Update"
        anchors.verticalCenterOffset: 119
        anchors.verticalCenter: parent.verticalCenter
        font.family: "Roboto"
        enabled: false

        onClicked: {
            textDebug.text = "Updating..."
            UpdateHandler.startUpdate()
            progressBar.visible = true
            checkButton.enabled = false
            updateButton.enabled = false
        }

        contentItem: Item {
            id: esItem1
            Text {
                color: updateButton.down ? "white" : "white"
                text: "Update"
                font.family: "Roboto"
                horizontalAlignment: Text.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 19
                verticalAlignment: Text.AlignVCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.weight: Font.Light
            }
        }
        anchors.topMargin: 38
        background: Rectangle {
            width: parent.width
            height: parent.height
            radius: 25
            border.color: "#3846b0"
            layer.enabled: true
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#4253d0"
                }

                GradientStop {
                    position: 1
                    color: "#3744ac"
                }
            }
            layer.effect: DropShadow {
                color: "#66111e71"
                radius: 25
                horizontalOffset: 0
                samples: 32
                verticalOffset: 8
            }
        }
        font.weight: "Light"
    }

    Button {
        id: checkButton
        x: 80
        y: 244
        width: 174
        height: 50
        text: "Check"
        anchors.verticalCenterOffset: 119
        anchors.verticalCenter: parent.verticalCenter
        anchors.topMargin: 38
        font.family: "Roboto"
        font.weight: "Light"

        onClicked: {
            textDebug.text = "Checking for update..."
            UpdateHandler.checkUpdate()
            checkButton.enabled = false
        }

        background: Rectangle {
            width: parent.width
            height: parent.height
            radius: 25
            border.color: "#3846b0"
            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    color: "#4253d0"
                }
                GradientStop {
                    position: 1.0
                    color: "#3744ac"
                }
            }

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 0
                verticalOffset: 8
                radius: 25
                color: "#66111e71"
                samples: 32
            }
        }

        contentItem: Item {
            id: esItem

            Text {
                color: checkButton.down ? "white" : "white"
                text: "Check"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 19
                font.weight: Font.Light
                font.family: "Roboto"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
    }



}















