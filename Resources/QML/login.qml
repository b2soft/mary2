import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import org.b2soft.qml 1.0

Page {
    id: page
    width: 700
    height: 500

    Keys.onReturnPressed: {
        loginButton.clicked()
        event.accepted = true
    }

    FastBlur {
        id: blur
        z: 1
        anchors.fill: parent
        source: mainItem
        radius: 0
    }

    Item {
        id: mainItem
        anchors.fill: parent

        state: "main"

        states: [
            State {
                name: "main"
                PropertyChanges {
                    target: blur
                    radius: 0
                }
            },
            State {
                name: "blurred"
                PropertyChanges {
                    target: blur
                    radius: 32
                }
            }
        ]

        transitions: [
            Transition {
                from: "main"
                to: "blurred"
                NumberAnimation {
                    property: "radius"
                    duration: 1000
                }
            },
            Transition {
                from: "blurred"
                to: "main"
                NumberAnimation {
                    property: "radius"
                    duration: 50
                }
            }
        ]

        Rectangle {
            id: rectangle
            width: 700
            height: 500
            color: "#ffffff"
            border.width: 0
        }

        Text {
            id: text4
            x: 351
            y: 420
            text: qsTr("Forgot password?")
            horizontalAlignment: Text.AlignRight
            font.family: "Roboto Light"
            font.pixelSize: 16
        }

        Button {
            id: loginButton
            objectName: "loginButton"
            onClicked: {
                mainItem.state = "blurred"
                UserHandler.logIn(loginInput.text, passwordInput.text)
            }
            x: 223
            y: 340
            width: 255
            text: qsTr("Next >")
            Layout.preferredWidth: 255
            Layout.fillHeight: false
            Layout.fillWidth: false
            font.pointSize: 20
            font.family: "Roboto"
            font.weight: "Light"

            background: Rectangle {
                width: parent.width
                height: parent.height
                radius: 25
                border.color: "#3846b0"
                gradient: Gradient {
                    GradientStop {
                        position: 0.0
                        color: "#4253d0"
                    }
                    GradientStop {
                        position: 1.0
                        color: "#3744ac"
                    }
                }

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 0
                    verticalOffset: 8
                    radius: 25
                    color: "#66111e71"
                    samples: 32
                }
            }

            contentItem: Text {
                text: loginButton.text
                font: loginButton.font
                opacity: enabled ? 1.0 : 0.3
                color: loginButton.down ? "white" : "white"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
            }
        }

        Text {
            id: statusText
            x: 227
            y: 111
            width: 250
            height: 28
            color: "#777777"
            text: qsTr("Enter your username and password to continue work")
            font.family: "Roboto Light"
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            elide: Text.ElideNone
            font.pixelSize: 16

            Connections {
                target: UserHandler
                onLoginFailed: {
                    mainItem.state = "main"
                    statusText.text = qsTr("Wrong username or password")
                    statusText.color = "#ff0000"
                }
            }
        }

        Text {
            id: text1
            x: 227
            y: 80
            text: qsTr("Log In")
            font.family: "Roboto Regular"
            font.pixelSize: 18
        }

        TextField {
            id: loginInput
            x: 227
            y: 180
            width: 250
            height: 44
            text: UserHandler.username
            placeholderText: qsTr("Username")
            font.pointSize: 18
            font.family: "Roboto Light"

            selectByMouse: true

            background: Rectangle {
                width: parent.width
                height: parent.activeFocus ? 2 : 1
                anchors.bottom: parent.bottom
                color: parent.activeFocus ? "#3a95db" : "#b6bac1"
            }
        }

        TextField {
            id: passwordInput
            x: 227
            y: 260
            width: 250
            height: 44
            text: UserHandler.hashedPassword
            placeholderText: qsTr("Password")
            font.pointSize: 18
            font.family: "Roboto Light"
            echoMode: TextInput.Password

            selectByMouse: true

            background: Rectangle {
                width: parent.width
                height: parent.activeFocus ? 2 : 1
                anchors.bottom: parent.bottom
                color: parent.activeFocus ? "#3a95db" : "#b6bac1"
            }
        }

        Text {
            id: text3
            x: 227
            y: 420
            text: qsTr("Register")
            font.family: "Roboto Light"
            font.pixelSize: 16
        }
    }
}

