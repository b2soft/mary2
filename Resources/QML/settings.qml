import QtQuick 2.11
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.3

import org.b2soft.qml 1.0

Page {
    id: page
    width: 700
    height: 360

    Rectangle {
        id: rectangle
        width: 700
        height: 360
        color: "#ffffff"
        border.width: 0
    }



    Item {
        id: buttonsItem
        width: 200
        height: 120
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

    }

    Item {
        id: esPathContainer
        width: 540
        height: 55
        anchors.top: parent.top
        anchors.topMargin: 64
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: text2
            color: "#6d6d6d"
            text: qsTr("Euroscope Path")
            font.weight: Font.Light
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            font.family: "Roboto"
            font.pixelSize: 14
        }

        Image {
            id: image
            width: 16
            height: 16
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.left: text2.right
            anchors.leftMargin: 11
            source: "qrc:/img/about.png"
        }

        Item {
            id: esRowContainer
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.top: text2.bottom
            anchors.topMargin: 10


            Rectangle {
                id: rectangle1
                radius: 3
                anchors.fill: parent
                border.color: "#9da5ae"
            }

            Item {
                id: item1
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 450
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0


                Rectangle {
                    id: rectangle3
                    color: "#9da5ae"
                    radius: 0
                    anchors.right: parent.right
                    anchors.rightMargin: 3
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top
                    anchors.leftMargin: 0
                    border.color: "#9da5ae"
                }


                Rectangle {
                    id: rectangle4
                    color: "#9da5ae"
                    radius: 3
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.leftMargin: -3
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: rectangle3.right
                    border.color: "#9da5ae"
                }


                Text {
                    id: text4
                    color: "#ffffff"
                    text: qsTr("Open")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.bottom: parent.bottom
                    font.weight: Font.Normal
                    font.family: "Roboto"
                    font.pixelSize: 17
                }

                MouseArea {
                    id: maChooseEuroscopeExe
                    anchors.fill: parent

                    onClicked: {
                        esPathDialog.visible = true
                    }
                }

            }

            Text {
                id: esPathText
                text: ( SettingsHandler.euroscopePath === "" ? "Empty" : SettingsHandler.natifyUrl(SettingsHandler.euroscopePath ) )
                anchors.right: parent.right
                anchors.rightMargin: 100
                font.weight: Font.Light
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter
                font.family: "Roboto"
                font.pixelSize: 16
            }
        }


    }

    Item {
        id: sectorPathContainer
        width: 540
        height: 55
        anchors.top: esPathContainer.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            id: text3
            color: "#6d6d6d"
            text: qsTr("Sector Path")
            anchors.leftMargin: 0
            anchors.top: parent.top
            font.family: "Roboto"
            font.pixelSize: 14
            font.weight: Font.Light
            anchors.topMargin: 0
            anchors.left: parent.left
        }

        Image {
            id: image1
            width: 16
            height: 16
            anchors.leftMargin: 11
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.left: text3.right
            source: "qrc:/img/about.png"
        }

        Item {
            id: sectorRowContainer
            anchors.leftMargin: 0
            anchors.top: text3.bottom
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.topMargin: 10
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            Rectangle {
                id: rectangle2
                radius: 3
                anchors.fill: parent
                border.color: "#9da5ae"
            }

            Item {
                id: item2
                anchors.leftMargin: 450
                anchors.top: parent.top
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                Rectangle {
                    id: rectangle5
                    color: "#9da5ae"
                    radius: 0
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.rightMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    border.color: "#9da5ae"
                    anchors.right: parent.right
                }

                Rectangle {
                    id: rectangle6
                    color: "#9da5ae"
                    radius: 3
                    anchors.leftMargin: -3
                    anchors.top: parent.top
                    anchors.rightMargin: 0
                    anchors.bottom: parent.bottom
                    anchors.left: rectangle5.right
                    border.color: "#9da5ae"
                    anchors.right: parent.right
                }

                Text {
                    id: text5
                    color: "#ffffff"
                    text: qsTr("Open")
                    font.family: "Roboto"
                    font.pixelSize: 17
                    font.weight: Font.Normal
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.bottom: parent.bottom
                    horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                MouseArea {
                    id: maChooseSectorFolder
                    anchors.fill: parent
                    onClicked: {
                        sectorPathDialog.visible = true
                    }
                }
                anchors.right: parent.right
            }

            Text {
                id: sectorPathText
                text: ( SettingsHandler.sectorPath === "" ? "Empty" : SettingsHandler.natifyUrl(SettingsHandler.sectorPath ) )
                anchors.right: parent.right
                anchors.rightMargin: 100
                font.pixelSize: 16
                anchors.left: parent.left
                anchors.leftMargin: 10
                font.weight: Font.Light
                font.family: "Roboto"
                anchors.verticalCenter: parent.verticalCenter
            }
            anchors.right: parent.right
        }
    }

    Button {
        id: saveButton
        width: 174
        height: 50
        visible: false
        anchors.top: sectorPathContainer.bottom
        anchors.topMargin: 38
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: "Roboto"
        font.weight: "Light"

        background: Rectangle {
            width: parent.width
            height: parent.height
            radius: 25
            border.color: "#3846b0"
            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    color: "#4253d0"
                }
                GradientStop {
                    position: 1.0
                    color: "#3744ac"
                }
            }

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 0
                verticalOffset: 8
                radius: 25
                color: "#66111e71"
                samples: 32
            }
        }

        contentItem: Item {
            id: esItem

            Text {
                color: saveButton.down ? "white" : "white"
                text: "Save"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 19
                font.weight: Font.Light
                font.family: "Roboto"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
    }



    Button {
        id: resetButton
        text: qsTr("Button")
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: saveButton.bottom
        anchors.topMargin: 30

        background: Item {

        }

        contentItem: Text {
            id: text1
            color: "#4d5457"
            text: qsTr("Reset to default")
            font.weight: Font.Light
            font.family: "Roboto Light"
            font.pixelSize: 14
        }
    }

    FileDialog {
        id: esPathDialog
        title: "Please specify the Euroscope.exe"
        folder: ( SettingsHandler.euroscopePath === "" ? SettingsHandler.getDefaultEuroscopePath() : SettingsHandler.euroscopePath )
        nameFilters: ["Euroscope executable (EuroScope.exe)"]
        onAccepted: {
            SettingsHandler.euroscopePath = esPathDialog.fileUrl
        }
    }

    FileDialog {
        id: sectorPathDialog
        title: "Please specify a folder to be used for a sector"
        folder: ( SettingsHandler.sectorPath === "" ? Qt.resolvedUrl("./") : SettingsHandler.sectorPath )
        selectFolder: true
        onAccepted: {
            SettingsHandler.sectorPath = sectorPathDialog.fileUrl
        }
    }
}


/*##^## Designer {
    D{i:108;anchors_height:100;anchors_width:100}D{i:110;anchors_height:100;anchors_width:100}
}
 ##^##*/
