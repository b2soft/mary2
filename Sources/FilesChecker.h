#pragma once

class FilesChecker : public QObject
{
	Q_OBJECT

public:

	enum class CheckType
	{
		k_exactMatch = 0,
		k_doNotTrack,
		k_parametersMatch
	};

	using FileRecordsMap = QHash<QString, std::pair<QString, CheckType>>;

	FilesChecker( QObject *parent, bool autoUpdateMode );

	void			RequestFilelist( const QString& filelistName );
	void			CalculateDifference();
	void			DownloadFiles();
	std::size_t		GetOutdatedFilesCount() const;
	void			SetTrackedPath( const QString& path ) noexcept;
	void			SetDownloadMode( const QString& mode ) noexcept;

signals:
	void			OneFileUpdated();
	void			UpdateFinished();
	void			FilesCompared();

private slots:
	void			OnReceivedFilelist( QNetworkReply* reply );
	void			OnDownloadReply( QNetworkReply* reply, QString filePath );

private:
	bool			IsFileUpToDate( const QString& fileName, const QString& remoteFileHash );
	void			WriteFile( QString fileName, QByteArray&& bytesToWrite );


	FileRecordsMap	m_remoteFileRecords;
	QSet<QString>	m_filesToDownload;
	QString			m_trackedPath;
	QString			m_downloadMode;
	bool			m_autoUpdateMode{ false };
};
