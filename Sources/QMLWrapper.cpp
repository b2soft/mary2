#include "Precompiled.h"

#include "QMLWrapper.h"
#include "Handlers/UserHandler.h"
#include "Handlers/ApplicationHandler.h"

QMLWrapper::QMLWrapper( QObject *parent ) :
	QObject( parent )
{
	auto connection = connect( ApplicationHandler::GetInstance(), &ApplicationHandler::selfUpdated, [this]
	{
		emit selfUpdated();
	} );
}
