#pragma once

#include <iostream>
#include <QObject>
#include <QDebug>

#include <QCoreApplication>
#include <QNetworkReply>

#include <QDir>
#include <QFile>
#include <QFileInfo>

#include <QTimer>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

const QString k_SiteAddress = "https://api.b2soft.org/mary-beta";