#include "precompiled.h"

#include <QDataStream>
#include <QDirIterator>
#include <QCryptographicHash>

#include "FilesChecker.h"

#include "Handlers/ApplicationHandler.h"
#include "Utils/RequestSender.h"

FilesChecker::FilesChecker( QObject *parent, bool autoUpdateMode ) :
	QObject( parent ),
	m_autoUpdateMode( autoUpdateMode )
{
}

void FilesChecker::CalculateDifference()
{
	QDir rootDir( m_trackedPath );
	QDirIterator it( m_trackedPath, QDir::Files, QDirIterator::Subdirectories );

	QSet<QString> localFilePaths;


	auto str = QCoreApplication::applicationDirPath();
	while( it.hasNext() )
	{
		auto fileName = it.next();
		localFilePaths.insert( fileName );
	}

	// Server-only files and Exact match
	QSet<QString> remoteFilePaths{ QSet<QString>::fromList( m_remoteFileRecords.keys() ) };


	// 1. Iterate all local files.
	for( auto it = localFilePaths.cbegin(); it != localFilePaths.cend(); )
	{
		QString localFileName = rootDir.relativeFilePath( *it );
		//qDebug() << localFileName;

		// 1.1 Delete all *.delete files
		if( localFileName.endsWith( ".delete" ) )
		{
			QFile::remove( *it );
			it = localFilePaths.erase( it );
			continue;
		}


		// // 1.2 Check local->remote match
		auto remoteFileIterator = remoteFilePaths.constFind( localFileName );

		if( remoteFileIterator != remoteFilePaths.end() )
		{
			switch( m_remoteFileRecords[*remoteFileIterator].second )
			{
				// 1.2.a If we need exact match, test file, download if outdated
				case CheckType::k_exactMatch:
				{
					if( !IsFileUpToDate( *it, m_remoteFileRecords[*remoteFileIterator].first ) )
					{
						m_filesToDownload.insert( localFileName );
					}
				}
				break;
				// 1.2.b If we need parameters match, download parameters, apply.
				case CheckType::k_parametersMatch:
				{
					//TODO
				}
				break;
			}

			// 1.2.c Remove file from both lists
			it = localFilePaths.erase( it );
			remoteFilePaths.erase( remoteFileIterator );
		}
		else
		{
			++it;
		}
	}

	// 2. Arm for download all remaining server-side files.
	for( auto it = remoteFilePaths.cbegin(); it != remoteFilePaths.cend(); ++it )
	{
		m_filesToDownload.insert( *it );
	}

	emit FilesCompared();

	// 3. Download all needed files and apply changes in auto update mode.
	if( m_autoUpdateMode )
	{
		DownloadFiles();
	}
}

void FilesChecker::RequestFilelist( const QString& filelistName )
{
	m_remoteFileRecords.clear();
	m_filesToDownload.clear();

	RequestSender* rs = new RequestSender( this );
	auto requestConnection = connect( rs, &RequestSender::onResponse, this, &FilesChecker::OnReceivedFilelist );
	rs->CreateRequest( k_SiteAddress + "/" + filelistName, {} );
}

void FilesChecker::SetTrackedPath( const QString& path ) noexcept
{
	m_trackedPath = path;
}

void FilesChecker::SetDownloadMode( const QString& mode ) noexcept
{
	m_downloadMode = mode;
}

bool FilesChecker::IsFileUpToDate( const QString& fileName, const QString& remoteFileHash )
{
	QFile file{ fileName };

	if( file.open( QFile::ReadOnly ) )
	{
		QByteArray data = file.readAll();
		QString hashString{ QCryptographicHash::hash( data, QCryptographicHash::Sha1 ).toHex() };

		return hashString == remoteFileHash;
	}

	return true;
}

void FilesChecker::DownloadFiles()
{
#if !defined( MARY_RELEASE )
	m_filesToDownload.erase( m_filesToDownload.find( "Mary.exe" ) );
#endif

	qDebug() << m_filesToDownload.size();

	if( m_filesToDownload.empty() )
	{
		emit UpdateFinished();
	}

	for( const QString& name : m_filesToDownload )
	{
		RequestSender* rs = new RequestSender( this );
		auto requestConnection = connect( rs, &RequestSender::onResponse, this, std::bind( &FilesChecker::OnDownloadReply, this, std::placeholders::_1, name ) );
		rs->CreateRequest( k_SiteAddress + "/downloadFile.php", { { "file", name }, { "mode", m_downloadMode } } );
	}
}

std::size_t FilesChecker::GetOutdatedFilesCount() const
{
	return m_filesToDownload.size();
}

void FilesChecker::WriteFile( QString fileName, QByteArray&& bytesToWrite )
{
	QFile file( fileName );
	QFileInfo fileInfo( file );

	QDir makeFolder( fileInfo.absoluteDir().path() );

	if( !makeFolder.exists() )
	{
		makeFolder.mkpath( fileInfo.absoluteDir().path() );
	}

	file.open( QFile::WriteOnly );
	QDataStream outFile( &file );
	outFile.writeRawData( bytesToWrite, bytesToWrite.length() );
	file.close();

	emit OneFileUpdated();
}

void FilesChecker::OnDownloadReply( QNetworkReply* reply, QString filePath )
{
	//auto r = reply->error();
	if( reply->error() != QNetworkReply::NoError )
	{
		return;
	}

	if( !reply->hasRawHeader( "Content-Disposition" ) )
	{
		return;
	}

	QByteArray replyByteArray = reply->readAll();
	QString fileName = m_trackedPath + "/" + filePath;

	QFile file( fileName );
	const bool writeable = file.open( QFile::WriteOnly );
	if( writeable )
	{
		file.close();
	}

	if( !writeable )
	{
		ApplicationHandler::GetInstance()->ArmForRestart();
		QFile::rename( fileName, fileName + ".delete" );
		WriteFile( fileName, std::move( replyByteArray ) );
	}
	else
	{
		WriteFile( fileName, std::move( replyByteArray ) );
	}

	m_filesToDownload.remove( filePath );

	if( m_filesToDownload.empty() )
	{
		emit UpdateFinished();
	}
}

void FilesChecker::OnReceivedFilelist( QNetworkReply* reply )
{
	if( reply->error() != QNetworkReply::NoError )
	{
		return;
	}

	QByteArray replyByteArray = reply->readAll();
	QJsonDocument jsonResponse = QJsonDocument::fromJson( replyByteArray );
	const QJsonArray json = jsonResponse.array();

	for( const QJsonValue& item : json )
	{
		QJsonObject obj = item.toObject();
		QString fileName = obj["path"].toString();
		QString hash = obj["hash"].toString();
		QString modeString = obj["mode"].toString();

		auto getMode = [&]( const QString& modeStr )->FilesChecker::CheckType
		{
			if( modeStr == "EXT" )
			{
				return FilesChecker::CheckType::k_exactMatch;
			}
			if( modeStr == "DNT" )
			{
				return FilesChecker::CheckType::k_doNotTrack;
			}
			if( modeStr == "PAM" )
			{
				return FilesChecker::CheckType::k_parametersMatch;
			}

			return FilesChecker::CheckType::k_doNotTrack;
		};

		FilesChecker::CheckType checkType = getMode( modeString );
		m_remoteFileRecords[fileName] = { hash, checkType };
	}

	CalculateDifference();
}

