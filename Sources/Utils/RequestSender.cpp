#include "Precompiled.h"

#include <QUrlQuery>
#include <QNetworkAccessManager>

#include "RequestSender.h"
#include "NetworkWrapper.h"


RequestSender::RequestSender( QObject* parent ) :
	QObject( parent ),
	m_nam( &NetworkWrapper::Instance() )
{
	m_connection = connect( m_nam, &QNetworkAccessManager::finished, this, &RequestSender::endRequest );
}

RequestSender::~RequestSender()
{
	disconnect( m_connection );
}

void RequestSender::CreateRequest( QString address, QHash<QString, QString> params )
{
	QUrlQuery postData;
	for( const QString& key : params.keys() )
	{
		postData.addQueryItem( key, params[key] );
	}

	QNetworkRequest request{ address };
	request.setOriginatingObject( this );
	request.setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );

	m_nam->post( request, postData.toString( QUrl::FullyEncoded ).toUtf8() );
}

void RequestSender::endRequest( QNetworkReply* reply )
{
	if( reply->request().originatingObject() != this )
	{
		return;
	}

	emit onResponse( reply );

	reply->deleteLater();
	this->deleteLater();
}
