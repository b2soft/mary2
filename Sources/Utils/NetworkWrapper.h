#pragma once

#include <QNetworkAccessManager>

class NetworkWrapper
{
public:
	static QNetworkAccessManager& Instance()
	{
		static QNetworkAccessManager instance;
		return instance;
	}

	NetworkWrapper() = delete;
	NetworkWrapper( const NetworkWrapper& ) = delete;
	NetworkWrapper& operator=( const NetworkWrapper& ) = delete;

	NetworkWrapper( NetworkWrapper&& ) = delete;
	NetworkWrapper& operator=( NetworkWrapper&& ) = delete;
};
