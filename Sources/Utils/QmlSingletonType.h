#pragma once

#include <QQmlEngine>
#include <QJSEngine>

template <class T>
class QmlSingletonType : public QObject
{
public:
	static T* GetInstance()
	{
		return m_instance;
	}

	static QObject* InstanceProvider( QQmlEngine*, QJSEngine* )
	{
		return static_cast<QObject*>( GetInstance() );
	}

	static void	Register( const char* name )
	{
		qmlRegisterSingletonType<T>( "org.b2soft.qml", 1, 0, name, InstanceProvider );
	}

	QmlSingletonType( const QmlSingletonType& ) = delete;
	QmlSingletonType& operator=( const QmlSingletonType& ) = delete;

protected:
	QmlSingletonType()
	{
		m_instance = static_cast<T*>( this );
	};

private:
	static T* m_instance;

};

template <class T>
T* QmlSingletonType<T>::m_instance = nullptr;