#pragma once

class QNetworkReply;
class QNetworkAccessManager;

class RequestSender : public QObject
{
	Q_OBJECT

public:
	RequestSender( QObject* parent = nullptr );
	~RequestSender();

	void					CreateRequest( QString address, QHash<QString, QString> params );

public slots:
	void					endRequest( QNetworkReply* reply );

signals:
	void					onResponse( QNetworkReply* reply );

private:
	QNetworkAccessManager*	m_nam{ nullptr };
	QMetaObject::Connection	m_connection;
};