#include "precompiled.h"

#include "utils/RequestSender.h"

#include "Handlers/SettingsHandler.h"

#include "FilesChecker.h"

#include "UpdateHandler.h"

UpdateHandler::UpdateHandler() noexcept
{
	m_filesChecker = std::make_unique<FilesChecker>( this, false );
}

void UpdateHandler::checkUpdate()
{
	auto sectorPath = SettingsHandler::GetInstance()->getSectorPath();
	if( sectorPath == "" || !QDir( SettingsHandler::GetInstance()->natifyUrl( sectorPath ) ).exists() )
	{
		emit sectorFolderError();
		return;
	}

	m_filesToUpdate = 0;
	auto connection = connect( m_filesChecker.get(), &FilesChecker::FilesCompared, [this]()
	{
		m_filesToUpdate = m_filesChecker->GetOutdatedFilesCount();
		emit checkoutComplete( m_filesToUpdate > 0 );
	} );

	auto conn2 = connect( m_filesChecker.get(), &FilesChecker::OneFileUpdated, [this]()
	{
		emit updateProgress( m_filesToUpdate - m_filesChecker->GetOutdatedFilesCount(), m_filesToUpdate );
	} );

	auto conn3 = connect( m_filesChecker.get(), &FilesChecker::UpdateFinished, [this]()
	{
		emit updateCompleted();
	} );

	m_filesChecker->SetTrackedPath( SettingsHandler::GetInstance()->natifyUrl( SettingsHandler::GetInstance()->getSectorPath() ) );
	m_filesChecker->SetDownloadMode( "sector" );
	m_filesChecker->RequestFilelist( "checkSector.php" );
}

void UpdateHandler::startUpdate()
{
	m_filesChecker->DownloadFiles();
}
