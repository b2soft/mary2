#pragma once

#include "Utils/QmlSingletonType.h"

class FilesChecker;

class ApplicationHandler : public QmlSingletonType<ApplicationHandler>
{
	Q_OBJECT

public:
	ApplicationHandler();

	static void		RegisterTypes();
	void			ArmForRestart();

public slots:
	void			startEuroscope();

signals:
	void			selfUpdated(); //for QMLWrapper

private slots:
	void			OnSelfUpdateFinished();

private:
	void			Restart();

	bool			m_isRestartNeeded{ false };

	FilesChecker*	m_selfChecker{ nullptr };
};
