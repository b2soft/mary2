#include "precompiled.h"

#include "SettingsHandler.h"

bool SettingsHandler::LoadSettings()
{
	QFile settingsFile( QCoreApplication::applicationDirPath() + QDir::separator() + "settings.dat" );
	if( !settingsFile.open( QIODevice::ReadOnly ) )
	{
		qWarning( "Could not open settings file." );
		return false;
	}

	QByteArray saveData = settingsFile.readAll();

	QJsonDocument loadDoc( QJsonDocument::fromBinaryData( saveData ) );

	Read( loadDoc.object() );

	return true;
}

bool SettingsHandler::SaveSettings()
{
	QFile settingsFile( QCoreApplication::applicationDirPath() + QDir::separator() + "settings.dat" );
	if( !settingsFile.open( QIODevice::WriteOnly ) )
	{
		qWarning( "Could not open settings file." );
		return false;
	}

	QJsonObject saveJsonObject;
	Write( saveJsonObject );

	QJsonDocument saveDoc( saveJsonObject );

	settingsFile.write( saveDoc.toBinaryData() );

	return true;
}

void SettingsHandler::Set( const QString& key, const QVariant& value )
{
	m_settings[key] = value;
}

bool SettingsHandler::Has( const QString& key )
{
	return m_settings.contains( key );
}

void SettingsHandler::Remove( const QString& key )
{
	if( m_settings.contains( key ) )
	{
		m_settings.remove( key );
	}
}

QString SettingsHandler::getEuroscopePath() const
{
	if( m_settings.contains( "euroscopePath" ) )
	{
		return m_settings["euroscopePath"].toString();
	}

	return {};
}

void SettingsHandler::setEuroscopePath( const QString& newEuroscopePath )
{
	Set( "euroscopePath", newEuroscopePath );
	SaveSettings();

	emit euroscopePathChanged( newEuroscopePath );
}

QString SettingsHandler::getSectorPath() const
{
	if( m_settings.contains( "sectorPath" ) )
	{
		return m_settings["sectorPath"].toString();
	}

	return {};
}

void SettingsHandler::setSectorPath( const QString& newSectorPath )
{
	Set( "sectorPath", newSectorPath );
	SaveSettings();

	emit sectorPathChanged( newSectorPath );
}

Q_INVOKABLE QString SettingsHandler::getDefaultEuroscopePath()
{
	return QString{ qgetenv( "programfiles" ) } +QDir::separator() + "EuroScope";
}

Q_INVOKABLE QString SettingsHandler::natifyUrl( const QString& urlString )
{
	const QUrl url( urlString );
	if( url.isLocalFile() )
	{
		return QDir::toNativeSeparators( url.toLocalFile() );
	}
	return urlString;
}

const QVariant& SettingsHandler::Get( const QString& key )
{
	return m_settings[key];
}

void SettingsHandler::Read( const QJsonObject& json )
{
	m_settings = json.toVariantHash();
}

void SettingsHandler::Write( QJsonObject& json ) const
{
	json = QJsonObject::fromVariantHash( m_settings );
}
