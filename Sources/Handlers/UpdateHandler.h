#pragma once

#include "Utils/QmlSingletonType.h"

class FilesChecker;

class UpdateHandler : public QmlSingletonType<UpdateHandler>
{
	Q_OBJECT

		Q_PROPERTY( QString debugText MEMBER m_debugText NOTIFY debugTextChanged )

public:
	UpdateHandler() noexcept;
public slots:
	void		checkUpdate();
	void		startUpdate();

signals:
	//qml notifiers
	void		debugTextChanged( const QString& newDebugText );

	//qml signals
	void		checkoutComplete( const bool needsUpdate );
	void		updateProgress( unsigned int current, unsigned int count );
	void		updateCompleted();
	void		sectorFolderError();

private slots:
	//void		OnLogInReply( QNetworkReply* reply );

private:
	QString							m_debugText;
	std::unique_ptr<FilesChecker>	m_filesChecker;
	std::size_t						m_filesToUpdate{ 0 };
};
