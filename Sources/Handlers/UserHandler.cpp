#include "precompiled.h"

#include "utils/RequestSender.h"

#include "Handlers/SettingsHandler.h"

#include "UserHandler.h"

QString UserHandler::getUsername()
{
	auto settingsManager = SettingsHandler::GetInstance();
	if( settingsManager->Has( "username" ) )
	{
		return settingsManager->Get( "username" ).toString();
	}

	return {};
}

QString UserHandler::getHashedPassword()
{
	auto settingsManager = SettingsHandler::GetInstance();
	if( settingsManager->Has( "hashedPassword" ) )
	{
		return settingsManager->Get( "hashedPassword" ).toString();
	}

	return {};
}

void UserHandler::logIn( const QString& name, const QString& password )
{
	RequestSender* rs = new RequestSender( this );

	m_hashedPassword = password;

	auto settingsManager = SettingsHandler::GetInstance();
	if( !settingsManager->Has( "hashedPassword" ) )
	{
		EncodePassword( m_hashedPassword );
	}

	auto requestConnection = connect( rs, &RequestSender::onResponse, this, &UserHandler::OnLogInReply );
	rs->CreateRequest( k_SiteAddress + "/login.php", { { "vatsimid", name }, { "password", m_hashedPassword } } );
}

void UserHandler::OnLogInReply( QNetworkReply* reply )
{
	switch( reply->error() )
	{
		case QNetworkReply::NoError:
		{
			QJsonDocument jsonResponse = QJsonDocument::fromJson( reply->readAll() );
			QJsonObject json = jsonResponse.object();

			m_vatsimId = json["vid"].toString();
			m_rating = json["atc_rating"].toString();

			SettingsHandler::GetInstance()->Set( "username", m_vatsimId );
			SettingsHandler::GetInstance()->Set( "hashedPassword", m_hashedPassword );
			SettingsHandler::GetInstance()->SaveSettings();

			emit loginCompleted();
		}
		break;
		case QNetworkReply::AuthenticationRequiredError:
		{
			SettingsHandler::GetInstance()->Remove( "username" );
			SettingsHandler::GetInstance()->Remove( "hashedPassword" );
			SettingsHandler::GetInstance()->SaveSettings();

			emit loginFailed();
		}
		break;
	}
}

void UserHandler::EncodePassword( QString& password )
{
	password = QCryptographicHash::hash( password.toUtf8(), QCryptographicHash::Sha1 ).toHex();
}
