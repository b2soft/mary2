#pragma once

#include "Utils/QmlSingletonType.h"

class SettingsHandler : public QmlSingletonType<SettingsHandler>
{
	Q_OBJECT

		Q_PROPERTY( QString euroscopePath READ getEuroscopePath WRITE setEuroscopePath NOTIFY euroscopePathChanged )
		Q_PROPERTY( QString sectorPath READ getSectorPath WRITE setSectorPath NOTIFY sectorPathChanged )

public:
	bool			LoadSettings();
	bool			SaveSettings();

	void			Set( const QString& key, const QVariant& value );
	const QVariant&	Get( const QString& key );
	bool			Has( const QString& key );
	void			Remove( const QString& key );

	QString			getEuroscopePath() const;
	void			setEuroscopePath( const QString& newEuroscopePath );

	QString			getSectorPath() const;
	void			setSectorPath( const QString& newSectorPath );


	Q_INVOKABLE QString	getDefaultEuroscopePath();
	Q_INVOKABLE QString natifyUrl( const QString& urlString );

signals:
	void			euroscopePathChanged( const QString& newEuroscopePath );
	void			sectorPathChanged( const QString& newSectorPath );

private:
	void			Read( const QJsonObject& json );
	void			Write( QJsonObject& json ) const;


	QVariantHash	m_settings;
};
