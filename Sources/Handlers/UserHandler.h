#pragma once

#include "Utils/QmlSingletonType.h"

class QNetworkReply;

class UserHandler : public QmlSingletonType<UserHandler>
{
	Q_OBJECT

		Q_PROPERTY( QString vatsimId MEMBER m_vatsimId NOTIFY vatsimIdChanged )
		Q_PROPERTY( QString rating MEMBER m_rating NOTIFY ratingChanged )

		Q_PROPERTY( QString username READ getUsername NOTIFY usernameChanged )
		Q_PROPERTY( QString hashedPassword READ getHashedPassword NOTIFY hashedPasswordChanged )

public:
	QString		getUsername();
	QString		getHashedPassword();

public slots:
	void		logIn( const QString& name, const QString& password );

signals:
	//qml notifiers
	void		vatsimIdChanged( const QString& newVatsimId );
	void		ratingChanged( const QString& newRating );

	void		usernameChanged( const QString& newUsername );
	void		hashedPasswordChanged( const QString& newHashedPassword );

	//qml signals
	void		loginCompleted();
	void		loginFailed();

private slots:
	void		OnLogInReply( QNetworkReply* reply );

private:
	void		EncodePassword( QString& password );

	QString		m_vatsimId;
	QString		m_rating;

	QString		m_username;
	QString		m_hashedPassword;
};
