#include "precompiled.h"

#include <QProcess>
#include <QSettings>

#include "FilesChecker.h"
#include "Handlers/UserHandler.h"
#include "Handlers/SettingsHandler.h"
#include "Handlers/UpdateHandler.h"

#include "ApplicationHandler.h"


ApplicationHandler::ApplicationHandler()
{
	new UserHandler();
	new SettingsHandler();
	new UpdateHandler();

	SettingsHandler::GetInstance()->LoadSettings();

#if defined( MARY_DEBUG )
	QTimer::singleShot( 2000, [this]
	{
		emit selfUpdated();
	} );
#else
	m_selfChecker = new FilesChecker( nullptr, true );
	m_selfChecker->SetTrackedPath( QCoreApplication::applicationDirPath() );
	m_selfChecker->SetDownloadMode( "client" );
	auto connection = connect( m_selfChecker, &FilesChecker::UpdateFinished, this, &ApplicationHandler::OnSelfUpdateFinished );
	m_selfChecker->RequestFilelist( "checkClient.php" );
#endif
}

void ApplicationHandler::RegisterTypes()
{
	ApplicationHandler::Register( "ApplicationHandler" );
	UserHandler::Register( "UserHandler" );
	SettingsHandler::Register( "SettingsHandler" );
	UpdateHandler::Register( "UpdateHandler" );
}

void ApplicationHandler::ArmForRestart()
{
	m_isRestartNeeded = true;
}

void ApplicationHandler::startEuroscope()
{
	SettingsHandler* settingsHandler = SettingsHandler::GetInstance();

	auto esPath = settingsHandler->natifyUrl( settingsHandler->getEuroscopePath() );
	if( !esPath.isEmpty() && QFile( esPath ).exists() )
	{
		// Check if *.prf is available. If yes - write it to ES registry
		QString prfPath = settingsHandler->natifyUrl( settingsHandler->getSectorPath() ) + "\\Ukraine.prf";
		if( QFile( prfPath ).exists() )
		{
			QSettings registry( "HKEY_CURRENT_USER\\Software\\Csernak Gergely\\EuroScope\\Settings", QSettings::NativeFormat );
			registry.setValue( "LastProfile", prfPath );
		}

		// In any case - start Euroscope
		QProcess::startDetached( "\"" + esPath + "\"" );
	}
}

void ApplicationHandler::OnSelfUpdateFinished()
{
	if( m_isRestartNeeded )
	{
		m_isRestartNeeded = false;
		Restart();
	}
	else
	{
		emit selfUpdated();
	}
}

void ApplicationHandler::Restart()
{
	QCoreApplication::quit();
	QProcess::startDetached( "Mary.exe" );
}

