#include "Precompiled.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QFontDatabase>

#include "Handlers/ApplicationHandler.h"

#include "QMLWrapper.h"

static QMLWrapper* wrapper;
static QObject* getWrapper( QQmlEngine*, QJSEngine* )
{
	return wrapper;
}

// To prevent message "Invalid parameter passed to C runtime function." to be displayed in Debug:
// 1. Delete bearer/qgenericbearerd.dll
// 2. Wait until next full windows release (Current is 1809)
// Refer this bug comments: https://bugreports.qt.io/browse/QTBUG-70917

int main( int argc, char *argv[] )
{
#if defined( Q_OS_WIN )
	QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif
	// Hack to preload Shared Libraries
	auto defaultConfig = QSslConfiguration::defaultConfiguration();

	//Fix needed for dragging window since Qt 5.12
	qputenv( "QT_QPA_DISABLE_ENHANCED_MOUSE", "1" );

	QGuiApplication app( argc, argv );

	QQmlApplicationEngine engine;

	new ApplicationHandler();

	wrapper = new QMLWrapper( nullptr );

	ApplicationHandler::RegisterTypes();

	QFontDatabase::addApplicationFont( ":/fonts/Roboto-Light.ttf" );
	QFontDatabase::addApplicationFont( ":/fonts/Roboto-Medium.ttf" );
	QFontDatabase::addApplicationFont( ":/fonts/Roboto-Regular.ttf" );

	engine.setObjectOwnership( wrapper, QQmlEngine::CppOwnership );
	qmlRegisterSingletonType<QMLWrapper>( "org.b2soft.qml", 1, 0, "QMLWrapper", getWrapper );
	engine.load( QUrl( QStringLiteral( "qrc:/QML/main.qml" ) ) );

	return app.exec();
}
