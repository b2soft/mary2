#pragma once

#include <QObject>

class QMLWrapper : public QObject
{
	Q_OBJECT

public:
	explicit QMLWrapper( QObject *parent );

signals:
	void	selfUpdated();

};
